/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ADC_Input_Pin GPIO_PIN_0
#define ADC_Input_GPIO_Port GPIOA
#define ADC_PWM_Out_Pin GPIO_PIN_1
#define ADC_PWM_Out_GPIO_Port GPIOA
#define BT_TX_Pin GPIO_PIN_2
#define BT_TX_GPIO_Port GPIOA
#define BT_RX_Pin GPIO_PIN_3
#define BT_RX_GPIO_Port GPIOA
#define BT_RESET_Pin GPIO_PIN_4
#define BT_RESET_GPIO_Port GPIOA
#define BT_STAT_Pin GPIO_PIN_5
#define BT_STAT_GPIO_Port GPIOA
#define BT_OUT1_Pin GPIO_PIN_6
#define BT_OUT1_GPIO_Port GPIOA
#define BT_LED1_IN_Pin GPIO_PIN_7
#define BT_LED1_IN_GPIO_Port GPIOA
#define BT_PWRC_Pin GPIO_PIN_0
#define BT_PWRC_GPIO_Port GPIOB
#define USR_BTN3_Pin GPIO_PIN_1
#define USR_BTN3_GPIO_Port GPIOB
#define USR_BTN4_Pin GPIO_PIN_2
#define USR_BTN4_GPIO_Port GPIOB
#define TIM2_CH3_PWM_Pin GPIO_PIN_10
#define TIM2_CH3_PWM_GPIO_Port GPIOB
#define USR_BTN1_Pin GPIO_PIN_11
#define USR_BTN1_GPIO_Port GPIOB
#define USR_BTN2_Pin GPIO_PIN_12
#define USR_BTN2_GPIO_Port GPIOB
#define TIM15_CH2_PWM_Pin GPIO_PIN_15
#define TIM15_CH2_PWM_GPIO_Port GPIOB
#define TIM1_CH1_PWM_Pin GPIO_PIN_8
#define TIM1_CH1_PWM_GPIO_Port GPIOA
#define LIN_TX_Pin GPIO_PIN_9
#define LIN_TX_GPIO_Port GPIOA
#define LIN_SLP_N_Pin GPIO_PIN_6
#define LIN_SLP_N_GPIO_Port GPIOC
#define LIN_WAKE_N_Pin GPIO_PIN_7
#define LIN_WAKE_N_GPIO_Port GPIOC
#define LIN_RX_Pin GPIO_PIN_10
#define LIN_RX_GPIO_Port GPIOA
#define TIM1_CH4_PWM_Pin GPIO_PIN_11
#define TIM1_CH4_PWM_GPIO_Port GPIOA
#define TIM2_CH2_PWM_Pin GPIO_PIN_3
#define TIM2_CH2_PWM_GPIO_Port GPIOB
#define TIM1_CH3_PWM_Pin GPIO_PIN_6
#define TIM1_CH3_PWM_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
