/*
 * BT_Comm.h
 *
 *  Created on: Dec 28, 2023
 *      Author: ewars
 */

#ifndef INC_BT_COMM_H_
#define INC_BT_COMM_H_

#include "main.h"
#include "stdint-gcc.h"

/*
 *			BT communication scheme:
 * - UART (BT) message consists of following parts:
 * 		1. Header:		4 bytes
 * 			a) msgSize		:1 byte
 * 			b) msgCounter 	:1 byte
 * 			c) devID		:2 bytes
 *
 * 		2. Payload
 * 			a) MAX RX BUFFER size, configured for each message in Header
 *
 * 		3. CRC:			1 byte
 */

typedef enum {
	AT_cmd_VERSION = 0,
	AT_cmd_MAC,
	AT_cmd_RESET,
	AT_cmd_NAME,
	AT_cmd_BAUD,
	AT_cmd_TYPE,
	AT_cmd_DISCONNECT,
	AT_cmd_MAX
}AT_commands_e;

typedef enum {
	BL_cmd_MAX
}BL_commands_e;

void JDY_Init(UART_HandleTypeDef * huart);
void JDY_Cyclic(void);

void JDY_sendCommand_AT(AT_commands_e * command, uint8_t size);
void JDY_sendCommand(BL_commands_e command, uint8_t* extData); //TODO

void JDY_sendData(uint8_t * data, uint8_t size);

#endif /* INC_BT_COMM_H_ */
