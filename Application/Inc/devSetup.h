/*
 * devSetup.h
 *
 *  Created on: 21 paź 2023
 *      Author: ewars
 */

#ifndef INC_DEVSETUP_H_
#define INC_DEVSETUP_H_


#include <LIN_Drv.h>
#include "NVM.h"
#include "AppStatus.h"

#define ZONE_ADDRESS(zone)				((zone+1)*10)
#define ZONE_COLLAR_ADDRESS				10
#define ZONE_SHOULDERS_ADDRESS			20
#define ZONE_BACK_FRONT_ADDRESS			30
#define ZONE_BACK_HINT_ADDRESS			40
#define ZONE_HINT_ADDRESS				50

#define DEV_NOT_ACITVE_BUS_ID			63

/*
 * 	Satellite position definition;
 *	Bus Address pattern:
 *
 *	0x [XX] [xx xxxx]
 *	[XX]: 		address parity bits
 *	[xx xxxx]:	address bits (decimal 63)
 *
 *	Zone: 		is described by decimal number.
 *				Zone 5 addresses to all devices.
 *				Zone 6 indicates no assignment.
 *
 *
 *	Position:	is described by unit number
 *				Zone 6 and position 3 indicates no assignment.
 *
 *	i.e.:
 *	0x XX 00 1101; decimal 13: Zone 1(shoulders), Position 3(MidRight)
 *
 * 	Satellite list array:
 *
 * 					L0	ML1	M2	MR3	R4
 * 	----------------------------------
 * 	[Collar]		x	x	x	x	x
 * 	[Shoulders]		x	x	x	x	x
 * 	[Back_head]		x	x	x	x	x
 * 	[back_hint]		x	x	x	x	x
 * 	[Hint]			x	x	x	x	x
 *
 * */

typedef enum {
	devT_massger = 0,
	devT_magnetic,
	devT_heater,
	devT_ultrasonic,
	devT_MAX
} devType_e;

typedef enum {
	devP_Left = 0,
	devP_MidLeft,
	devP_Mid,
	devP_MidRight,
	devP_Right,
	devP_MAX_ALL
} devPos_e;

typedef enum {
	devZone_collar = 0,
	devZone_shoulders,
	devZone_backFront,
	devZone_backHint,
	devZone_hint,
	devZone_MAX_ALL
} devZone_e;

typedef enum {
	devSts_notInitialized = 0,
	devSts_Active,
	devSts_Error,
	devSts_MAX
} devSts_e;


typedef struct {
	devSts_e	devStatus;
	devZone_e	devZone;
	devPos_e	devPosition;
	devType_e	devType;
	uint8_t		devBusAddres;
	uint8_t		Led1_duty;
	uint8_t		Led2_duty;
	uint8_t		Dev_duty;
	uint8_t		reserved;
} devSattelite_s;


// ------------------------------------------------------------------------------------

void 		dev_LinSend(uint8_t devId, uint8_t command, uint8_t * dataP);
void 		dev_LinReceive(uint8_t devId, uint8_t command, uint8_t * dataP);

void 		dev_ScanForSatellites(void);
fxnResp_e 	dev_dismissSat(devZone_e devZone, devPos_e devPos);
fxnResp_e 	dev_setSatAddress(devZone_e devZone, devPos_e devPos);
void 		dev_InitSatAll_NVM(void);
void 		dev_InitSat_NVM(devZone_e devZone, devPos_e devPos);
fxnResp_e 	dev_UpdateSat_MDuty(devZone_e devZone, devPos_e devPos, uint8_t duty);
void 		dev_UpdateZoneMotorDuty(devZone_e devZone, uint8_t duty);

uint8_t 	dev_satFindAll(void);

//	-----------------------------------------------------------------------------------

#endif /* INC_DEVSETUP_H_ */
