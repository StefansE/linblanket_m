/*
 * NVM.h
 *
 *  Created on: 22.02.2019
 *      Author: ewars
 */

#ifndef MYTASKS_NVM_H_
#define MYTASKS_NVM_H_

#include "inttypes.h"
#include "NVM_DataId.h"

#define DEVICE_PROGRAM_SIZE		64
#define NVM_CONTAINER_SIZE		((DEVICE_PROGRAM_SIZE/8)-2)

#if DEVICE_PROGRAM_SIZE == 32

	typedef struct {
		NVM_DataId_e 	nvm_Id;
		uint8_t 		nvm_extnd;
		//uint16_t		nvm_data16;
		uint8_t			nvm_data[NVM_CONTAINER_SIZE];
} NVM_Entry_t;

#elif DEVICE_PROGRAM_SIZE == 64

	typedef struct {
			NVM_DataId_e	nvm_Id;
			uint8_t			nvm_extnd;
			uint8_t			nvm_data[NVM_CONTAINER_SIZE];
		} NVM_Entry_t;

#endif

void 	NVM_InitNVM(void);
void 	NVM_ReadAll(NVM_Entry_t * entry);
void 	NVM_ReadEntryById(NVM_DataId_e entryId, NVM_Entry_t * entry_p);
void 	NVM_ReadExtendedData(NVM_DataId_e entryId, uint8_t * data_p);
void 	NVM_WriteEntry(NVM_Entry_t * entry);
uint8_t NVM_GetDeviceEntryIndex(uint8_t device);

/***********************************************************
 * 			Reserved for advanced user
 * *********************************************************/
void 	NVM_ClearAllPages(void);

#endif /* MYTASKS_NVM_H_ */
