/*
 * LIN_Service.h
 *
 *  Created on: Jan 6, 2024
 *      Author: ewars
 */

#ifndef INC_LIN_SERVICE_H_
#define INC_LIN_SERVICE_H_

void LIN_Init(UART_HandleTypeDef* huart);
void LIN_Cyclic(void);

void LIN_SendCommand(uint8_t devId, uint8_t command, uint8_t* txData_p, uint8_t Size);

#endif /* INC_LIN_SERVICE_H_ */
