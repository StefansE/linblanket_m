/*
 * NVM_DataId.h
 *
 *  Created on: 24.02.2019
 *      Author: ewars
 */

#ifndef MYTASKS_NVM_DATAID_H_
#define MYTASKS_NVM_DATAID_H_


/*
 * 		Enumerator listing all NVM data entities
 *
 *		It also defines NVM data Id number
 *
 *		Currently 8 bit value - 256 NVM entities possible
 * */

typedef enum {
	nvm_Led_Duty = 0,
	nvm_Serial_Number = 1,
	nvm_ResistorTrim = 2,
	nvm_satConfig_1 = 3,
	nvm_satConfig_2 = 4,
	nvm_satConfig_3 = 5,
	nvm_satConfig_4 = 6,
	nvm_satConfig_5 = 7,
	nvm_satConfig_6 = 8,
	nvm_satConfig_7 = 9,
	nvm_satConfig_8 = 10,
	nvm_satConfig_9 = 11,
	nvm_satConfig_10 = 12,
	nvm_satConfig_11 = 13,
	nvm_satConfig_12 = 14,
	nvm_satConfig_13 = 15,
	nvm_satConfig_14 = 16,
	nvm_satConfig_15 = 17,
	nvm_satConfig_16 = 18,
	nvm_satConfig_17 = 19,
	nvm_satConfig_18 = 20,
	nvm_satConfig_19 = 21,
	nvm_satConfig_20 = 22,
	nvm_satConfig_21 = 23,
	nvm_satConfig_22 = 24,
	nvm_satConfig_23 = 25,
	nvm_satConfig_24 = 26,
	nvm_satConfig_25 = 27,
	NVM_EntryCount
} NVM_DataId_e;

#endif /* MYTASKS_NVM_DATAID_H_ */
