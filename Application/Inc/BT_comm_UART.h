/*
 * BT_comm_UART.h
 *
 *  Created on: Dec 27, 2023
 *      Author: ewars
 */

#ifndef INC_BT_COMM_UART_H_
#define INC_BT_COMM_UART_H_

#define OUT_LED				0x00UL
#define OUT_LED_POS 		(0x01UL << OUT_LED)
#define LED1_LED			0x01UL
#define LED1_LED_POS 		(0x01UL << LED1_LED)
#define STAT_LED			0x02UL
#define STAT_LED_POS 		(0x01UL << STAT_LED)
#define RX_NEW_DATA			0x03UL
#define RX_NEW_DATA_POS		(0x01UL << RX_NEW_DATA)
#define TX_NEW_DATA			0x04UL
#define TX_NEW_DATA_POS		(0x01UL << TX_NEW_DATA)
#define RX_DATA_EN			0x05UL
#define RX_DATA_EN_POS		(0x01UL << RX_DATA_EN)
#define TX_DATA_EN			0x06UL
#define TX_DATA_EN_POS		(0x01UL << TX_DATA_EN)
#define REQ_ACTIVE			0x07UL
#define REQ_ACTIVE_POS		(0x01UL << REQ_ACTIVE)

#define ASCII_CR 		13
#define ASCII_LF 		10
#define ASCII_PLUS		43

#define AT_RX_TIMEOUT	100

#define RX_BUFFER_SIZE	100
#define TX_BUFFER_SIZE	100

#define RX_DATA_SIZE	200
#define TX_DATA_SIZE	200


typedef enum {
	bt_state_IDLE = 0,
	bt_state_Sending,
	bt_state_Receiving,
	bt_state_MAX
} bt_state_e;

typedef enum {
	bt_mode_RESET = 0,
	bt_mode_AT,
	bt_mode_TRANSPARENT,
	bt_mode_MAX
} bt_mode_e;

typedef struct __BT_Dev_s{

	bt_mode_e				bt_mode;

	bt_state_e				bt_state;

	union {
		struct{
			uint8_t OUT1_Led		:1; /* LED Status*/
			uint8_t LED1_Led		:1; /* LED Status*/
			uint8_t STAT_Led		:1; /* LED Status*/

			uint8_t RX_NewData		:1; /* RX New data received */
			uint8_t TX_NewData		:1;	/* TX New data to be transmitted */
			uint8_t DataRx_DMA		:1; /* RX DMA transfer ongoing */
			uint8_t DataTx_DMA		:1; /* TX DMA transfer ongoing */

			uint8_t RequestActive	:1;

			uint8_t Reserved		:8;

		};
		uint16_t StatusRegister;
	} Status;


	uint8_t					TX_Xfer_Size;

	uint8_t					TX_Xfer_Count;

	uint8_t*				TX_Data_p;

	uint8_t*				TX_Buffer_p;

	uint8_t					RX_Xfer_Size;

	uint8_t					RX_Xfer_Count;

	uint8_t*				Rx_Data_p;

	uint8_t*				Rx_Buffer_p;

	uint32_t				BT_ErrorCode;

	UART_HandleTypeDef*		huart;

} BT_Dev_s;



void 		bt_Init(UART_HandleTypeDef * huart);
void 		bt_Cyclic();

// IRQ callback functions
void 		bt_UART_TxCpltCallback(UART_HandleTypeDef *huart);
void 		bt_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void 		bt_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size);

uint8_t 	bt_getNewRxFlag(void);
uint8_t 	bt_getNewTxFlag(void);

uint8_t 	bt_getRxDataSize(void);
uint8_t 	bt_getRxData(uint8_t * data);

bt_mode_e 	bt_getMode(void);
void 		bt_gotoMode(bt_mode_e mode);

void 		bt_sendDataAsync(uint8_t * data, uint8_t size);
void 		bt_sendData_DMA(uint8_t * data, uint8_t size);

void 		bt_AT_Receive(uint8_t * command, uint8_t cmdSize, uint8_t * response);
void 		bt_AT_sendCommand(uint8_t * command, uint8_t command_size);




#endif /* INC_BT_COMM_UART_H_ */
