/*
 * LIN_Service.h
 *
 *  Created on: 22 gru 2022
 *      Author: ewars
 */

#ifndef INC_LIN_DRV_H_
#define INC_LIN_DRV_H_

#include "usart.h"

#include "stdint-gcc.h"

#define LIN_RX_BUFFER_SIZE			20
#define LIN_TX_BUFFER_SIZE			20

#define LIN_DEFAULT_PAYLOAD_SIZE	4
#define LIN_DEFAULT_RECEIVE_SIZE	10

#define LIN_CMD_RESERVED			0

#define LIN_CMD_GET_STATUS			1
#define LIN_CMD_GET_MY_ADDRESS 		2
#define LIN_CMD_GET_OUT_DUTY		3
#define LIN_CMD_GET_LED1_DUTY 		4
#define LIN_CMD_GET_LED2_DUTY 		5
#define LIN_CMD_GET_BAT_VOLTAGE 	6
#define LIN_CMD_GET_SUP_VOLTAGE		7
#define LIN_CMG_GET_CHIP_ID_0		8
#define LIN_CMG_GET_CHIP_ID_1		9
#define LIN_CMG_GET_CHIP_ID_2		10

#define LIN_CMD_SET_MOTOR_DUTY		20
#define LIN_CMD_SET_LED1_DUTY		21
#define LIN_CMD_SET_LED2_DUTY		22
#define LIN_CMD_SET_MOTOR_INIT		23
#define LIN_CMD_SET_LED1_INIT		24
#define LIN_CMD_SET_LED2_INIT		25
#define LIN_CMD_SET_MY_ADRESS		26
#define LIN_CMD_SET_MY_ROLE			27

typedef enum {
	lin_st_INIT = 0,
	lin_st_IDLE,
	lin_st_Sending,
	lin_st_Receiving,
	lin_st_MAX
}linState_e;

typedef struct{
	uint8_t Synchronization_Byte_u8;
	uint8_t Identifier_u8;
	uint8_t MsgSize_u8;
	uint8_t Command_u8;
	uint8_t Checksum_u8;
}LIN_header_t;


typedef struct {
	uint8_t		stopByte;
	uint8_t		syncByte;	// 0x55
	uint8_t		devID;		// (xx)00 0000; xx-parity
	uint8_t		msgSize;	// set to 4
	uint8_t		command;
	uint8_t		msgChecksum;
	uint8_t		statusBits;
	uint8_t		motorDuty;
	uint8_t		led1Duty;
	uint8_t		led2Duty;
}devStatus_s;

typedef struct {
	uint8_t 	devID;
	uint8_t		commandID;
	uint8_t*	data_p;
} linMessage_s;


typedef struct {

	linState_e				State;

	struct {
		uint8_t 			RX_NewData	:1;
		uint8_t				TX_NewData	:1;

		uint8_t				RX_DMAxfer	:1;
		uint8_t				TX_DMAxfer	:1;

		uint8_t				Reserved	:4;
	}Status;

	uint8_t					TX_Xfer_Size;

	LIN_header_t			Tx_Header_Buffer;

	uint8_t*				TX_Buffer_p;

	LIN_header_t			Tx_Header_Data;

	uint8_t*				TX_Data_p;

	uint8_t					RX_Xfer_Size;

	uint8_t*				RX_Buffer_p;

	uint8_t*				RX_Data_p;

	uint32_t				LIN_ErrorCode;

	UART_HandleTypeDef*		huart;

} LinDev_s;


void 		LinDrv_Init(UART_HandleTypeDef* huart);
void 		LinDrv_Cyclic(void);

void 		LinDrv_SendDeviceCommand(linMessage_s message, uint8_t dataSize);
uint8_t 	LinDrv_GetResponse(uint8_t* data_p);

void 		Lin_UART_TxCpltCallback(UART_HandleTypeDef *huart);
void 		Lin_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void 		Lin_UARTEx_RxEventCallback(UART_HandleTypeDef* huart, uint16_t Size);



void LIN_GetDeviceStatus(uint8_t devId, devStatus_s * rxBuffer);

void LIN_WriteData(LIN_header_t *header, uint8_t * TXdata_p);
void LIN_ReadData(LIN_header_t *header, uint8_t * RXdata_p, uint8_t size);

#endif /* INC_LIN_DRV_H_ */
