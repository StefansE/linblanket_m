/*
 * Program_NVM.h
 *
 *  Created on: Nov 17, 2023
 *      Author: ewars
 */

#ifndef PROGRAM_NVM_H_
#define PROGRAM_NVM_H_

#include "stdint-gcc.h"

#define PAGER_HEADER_START			0x0801C000
#define P_CHEK_BIT (32BIT, BIT)		(32BIT & (1<<BIT))

#define PROGRAM_MAX_STEP_COUNT		100

/*
 * 	Satellite position definition;
 *
 * 	Satellite list array:
 *
 * 						L0	ML1	M2	MR3	R4
 * 	---------------------------------------
 * 	[nvmCollar]			x	x	x	x	x
 * 	[nvmShoulders]		x	x	x	x	x
 * 	[nvmBack_head]		x	x	x	x	x
 * 	[nvmBack_hint]		x	x	x	x	x
 * 	[nvmHint]			x	x	x	x	x
 *
 * */

typedef enum {
	nvmSat_Left = 0,
	nvmSat_MidLeft,
	nvmSat_Middle,
	nvmSat_MidRight,
	nvmSat_Right,
	nvmSat_Group_Massage,
	nvmSat_Group_Heater,
	nvmSat_Group_Magnetic,
	nvmSat_Group_UltraSound,
	nvmSat_MAX
} nvmAtuatorId_e;

typedef enum {
	nvmZone_Collar = 0,
	nvmZone_Shoulders,
	nvmZone_Back_head,
	nvmZone_Back_hint,
	nvmZone_Hint,
	nvmZone_MAX
} nvmZoneId_e;

typedef struct {
	uint8_t		stepId;
	uint8_t		dutyCycles[nvmZone_MAX][nvmSat_MAX];
	uint8_t		duration_seconds;
	uint8_t		timeout_seconds;
} stepConfig_s;


typedef struct {
	uint8_t				programId;
	uint8_t				stepCount;
	stepConfig_s *		firstStep_p;
	stepConfig_s *		lastStep_p;
	uint32_t *			nextProgram_p;
} pProgram_s;

void pNvm_initNvm(pProgram_s * runTimeProgramList);

void prog_LoadProg(pProgram_s * pProgram_p);
void prog_Cyclic(void);

void prog_Run(void);
void prog_Stop(void);

#endif /* PROGRAM_NVM_H_ */
