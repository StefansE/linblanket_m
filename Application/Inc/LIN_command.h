/*
 * LIN_command.h
 *
 *  Created on: 27 gru 2022
 *      Author: ewars
 */

#ifndef INC_LIN_COMMAND_H_
#define INC_LIN_COMMAND_H_

#include "stdint-gcc.h"

#define LIN_CMD_RESERVED			0

#define LIN_CMD_GET_STATUS			1
#define LIN_CMD_GET_MY_ADDRESS 		2
#define LIN_CMD_GET_OUT_DUTY		3
#define LIN_CMD_GET_LED1_DUTY 		4
#define LIN_CMD_GET_LED2_DUTY 		5
#define LIN_CMD_GET_BAT_VOLTAGE 	6
#define LIN_CMD_GET_SUP_VOLTAGE		7
#define LIN_CMG_GET_CHIP_ID_0		8
#define LIN_CMG_GET_CHIP_ID_1		9
#define LIN_CMG_GET_CHIP_ID_2		10
#define LIN_CMD_GET_TEMPERATURE		11

#define LIN_CMD_SET_MOTOR_DUTY		20
#define LIN_CMD_SET_LED1_DUTY		21
#define LIN_CMD_SET_LED2_DUTY		22
#define LIN_CMD_SET_MOTOR_INIT		23
#define LIN_CMD_SET_LED1_INIT		24
#define LIN_CMD_SET_LED2_INIT		25
#define LIN_CMD_SET_MY_ADRESS		26
#define LIN_CMD_SET_MY_ROLE			27
#define LIN_START_MOTOR				28
#define LIN_STOP_MOTOR				29
#define LIN_CMD_SET_RESISTOR_TRIM	30

#define LIN_ZONE_START				80
#define LIN_ZONE_STOP				81

/* --------------------------------------------------------
 * 					Getter Function prototypes
 * ------------------------------------------------------- */
void LIN_GetStatus(uint8_t * data_p);
void LIN_GetMyAddress(uint8_t * data_p);
void LIN_GetLED1InitDuty(uint8_t * data_p);
void LIN_GetLED2InitDuty(uint8_t * data_p);
void LIN_GetOutputInitDuty(uint8_t * data_p);
void LIN_GetBatVoltage(uint8_t * data_p);
void LIN_GetSupplyVoltage(uint8_t * data_p);
void LIN_GetChipID_0(uint8_t * data_p);
void LIN_GetChipID_1(uint8_t * data_p);
void LIN_GetChipID_2(uint8_t * data_p);
void LIN_GetTemperature(uint8_t * data_p);


/* --------------------------------------------------------
 * 					Setter Function prototypes
 * ------------------------------------------------------- */
void LIN_SetMotorDuty(uint8_t * data_p);
void LIN_SetLED1Duty(uint8_t * data_p);
void LIN_SetLED2Duty(uint8_t * data_p);

void LIN_SetMotorInitDuty(uint8_t * data_p);
void LIN_SetLED1InitDuty(uint8_t * data_p);
void LIN_SetLED2InitDuty(uint8_t * data_p);

void LIN_SetMyAddress(uint8_t * data_p);
void LIN_SetMyRole(uint8_t * data_p);

void LIN_StartMotor(uint8_t * data_p);
void LIN_StopMotor(uint8_t * data_p);

void LIN_SetResistorTrim(uint8_t * data_p);

/* --------------------------------------------------------
 * 					ZONE Function prototypes
 * ------------------------------------------------------- */
void LIN_ZONE_Start(uint8_t * data_p);
void LIN_ZONE_Stop(uint8_t * data_p);

#endif /* INC_LIN_COMMAND_H_ */
