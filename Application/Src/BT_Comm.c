/*
 * BT_Comm.c
 *
 *  Created on: Dec 30, 2023
 *      Author: ewars
 */


#include "BT_Comm.h"

#include "BT_comm_UART.h"

uint8_t BT_AT_VER[] = 			{"AT+VER"};
uint8_t BT_AT_MAC[] = 			{"AT+MAC"};
uint8_t BT_AT_RESET[] = 		{"AT+RESET"};
uint8_t BT_AT_NAME[] =			{"AT+NAME"};
uint8_t BT_AT_BAUD[] =			{"AT+BAUD"};
uint8_t BT_AT_TYPE[] =			{"AT+CLSS"};
uint8_t BT_AT_DISCO[] =			{"AT+DISC"};

typedef struct {
	union{
		struct {
			uint8_t msgSize;
			uint8_t msgCounter;
			uint8_t devID[2];
		};
		uint8_t header[4];
	} btHeader_u;

	uint8_t* pLoad_p;

	uint8_t bt_CRC;

} btMessage_s;

typedef struct {
	AT_commands_e 	command;
	uint8_t * 		string_p;
	uint8_t 		cmdSize;
}AT_Commands_s;

AT_Commands_s AT_Commands[] = {
		{AT_cmd_VERSION, 	BT_AT_VER, 		sizeof(BT_AT_VER)/sizeof(BT_AT_VER[0])},
		{AT_cmd_MAC,		BT_AT_MAC, 		sizeof(BT_AT_MAC)/sizeof(BT_AT_MAC[0])},
		{AT_cmd_RESET,		BT_AT_RESET, 	sizeof(BT_AT_RESET)/sizeof(BT_AT_RESET[0])},
		{AT_cmd_NAME,		BT_AT_NAME,		sizeof(BT_AT_NAME)/sizeof(BT_AT_NAME[0])},
		{AT_cmd_BAUD,		BT_AT_BAUD,		sizeof(BT_AT_BAUD)/sizeof(BT_AT_BAUD[0])},
		{AT_cmd_TYPE,		BT_AT_TYPE,		sizeof(BT_AT_TYPE)/sizeof(BT_AT_TYPE[0])},
		{AT_cmd_DISCONNECT,	BT_AT_DISCO, 	sizeof(BT_AT_DISCO)/sizeof(BT_AT_DISCO[0])}
};

uint8_t rxData_Buf[RX_DATA_SIZE];


void JDY_Init(UART_HandleTypeDef * huart){
	bt_Init(huart);
}

void JDY_Cyclic(void){
	bt_Cyclic();

	uint8_t rxDataSize = bt_getRxData(rxData_Buf);

	if(rxDataSize > 0){
		// Handle Message
		// Set up message pointer
		//btMessage_s* message = (btMessage_s*) rxData_Buf;

		// Check CRC

		// Check devId
		//TODO
	}
}

void JDY_sendCommand_AT(AT_commands_e * command, uint8_t size){
	bt_AT_sendCommand(command, size);
}

void JDY_sendCommand(BL_commands_e command, uint8_t* extData){
	//TODO
}

void JDY_sendData(uint8_t * data, uint8_t size){
	bt_sendDataAsync(data, size);
}
