/*
 * SytemCallbacks.c
 *
 *  Created on: Jan 6, 2024
 *      Author: ewars
 */

#ifndef SRC_SYTEMCALLBACKS_C_
#define SRC_SYTEMCALLBACKS_C_

#include "LIN_Drv.h"
#include "BT_comm_UART.h"


void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size){

	// Coming from LIN Receive to IDLE
	if(huart == &huart1){
		Lin_UARTEx_RxEventCallback(huart, Size);
	}

	// Coming from BT Receive to IDLE
	if(huart == &huart2){
		bt_UARTEx_RxEventCallback(huart, Size);
	}


}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){

	// huart1 - LIN
	if(&huart1 == huart){
		Lin_UART_RxCpltCallback(huart);
	}

	// huart2 BlueTooth UART
	if(huart == &huart2){
		bt_UART_RxCpltCallback(huart);
	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){

	// huart1 - LIN
	if(huart == &huart1){
		Lin_UART_TxCpltCallback(huart);
	}

	// huart2 BlueTooth UART
	if(huart == &huart2){
		bt_UART_TxCpltCallback(huart);
	}

}

#endif /* SRC_SYTEMCALLBACKS_C_ */
