/*
 * Application.c
 *
 *  Created on: 22 gru 2022
 *      Author: ewars
 */

#include "adc.h"
#include "usart.h"
#include "rtc.h"

#include "Application.h"
#include "AppStatus.h"
#include "devSetup.h"

#include "Program_NVM.h"
#include "BT_Comm.h"
#include "LIN_Service.h"

typedef enum {
	adc_Supply_33 = 0,
	adc_Battery,
	adc_Temperature,
	adc_Reference,
	adc_MAX
} adcChannels_e;

//	--------------	TIME	---------------------
RTC_TimeTypeDef startupTime = {0};
RTC_TimeTypeDef currentTime = {0};
RTC_DateTypeDef currentDate = {0};


//	-----------	ADC	------------------
#define V_REFL_ADDR 0x1FFF75AA
#define V_REFH_ADDR 0x1FFF75AB

uint16_t 	ADC_ConvertBatVoltage(uint16_t rawVal);
uint16_t 	ADC_ConvertSupVoltage(uint16_t rawVal);
void 		App_NVMInitiateApp(void);
void 		App_UpdateMainTimer_1s(void);
void 		sys_InitADC(void);

float refFactor = 0;
uint16_t vResistorValue_H = 62;

//	--------	APP Setup 	---------------------------------------


devList_t devList[] = {
		{&htim1,	TIM_CHANNEL_1},//LED 1
		{&htim1,	TIM_CHANNEL_3},//LED 2
		{&htim1,	TIM_CHANNEL_4},//LED 3
		{&htim15,	TIM_CHANNEL_2},//LED 4
		{&htim2,	TIM_CHANNEL_2},//LED 5
		{&htim2,	TIM_CHANNEL_3} //LED 6
};

appStatus_s	appStatus = {0};
appMainStatus_e appMainStatus = appMain_Init;

uint32_t 	mainSystemTick = 0;
uint8_t 	TaskSem_T3 = 0;

uint16_t ADC_Data[4] = {0};



//	###########   DEBUG		###############

#define APP_DEBUG

#ifdef APP_DEBUG

void App_RunDebugRoutine(void);

uint8_t LIN_TXBuffer[] = {21, 32, 43, 54};

LIN_header_t msgHeader = {0};
devStatus_s devStatus = {0};

uint8_t dbg_lin_msgSemaphore = 1;
uint8_t dbg_sem_SatDismiss = 0;
uint8_t dbg_sem_SatSetAddress = 0;
fxnResp_e dbg_resp;
devZone_e dbg_currentZone = devZone_collar;
devPos_e dbg_currentPos = devP_Left;
uint8_t dbg_satIdGlobal = 24;
uint8_t dbg_command = LIN_CMD_GET_STATUS;
uint8_t dbg_satCount = 0;

uint8_t dbg_busIdTable[3] = {11, 12, 34};
uint8_t dbg_delayCounter = 0;
uint8_t dbg_addresIdx = 0;

uint8_t dbg_btResp[50] = {0};

uint8_t dbg_setTimeFlag = 0;

uint32_t debugDelay = 0;
#define DEBG_DELAY	10

uint8_t dbg_btTaskCnt = 0;
uint8_t dbg_btTaskSem = 0;
uint8_t dbg_BTdataToSend[] = {1, 2, 3, 4};
//	-------------------------------------------------
#endif


void MyTask(void){

	while(1){

		switch(appMainStatus){

		case appMain_Init:

			startupTime.Hours = 12;
			startupTime.Minutes = 55;
			startupTime.Seconds = 0;
			startupTime.TimeFormat = RTC_FORMAT_BIN;

			//HAL_RTC_SetTime(&hrtc, &startupTime, RTC_FORMAT_BIN);
			HAL_RTCEx_EnableBypassShadow(&hrtc);


			NVM_InitNVM();
			App_NVMInitiateApp();


			//dev_ScanForSatellites();
			LIN_Init(&huart1);
			JDY_Init(&huart2);

			sys_InitADC();

			HAL_ADC_Stop(&hadc1);
			HAL_ADCEx_Calibration_Start(&hadc1);

#ifdef APP_DEBUG
			dbg_satCount = dev_satFindAll();
#endif

			appMainStatus = appMain_Run;
			break;

		case appMain_Run:

			JDY_Cyclic();
			LIN_Cyclic();

			App_UpdateMainTimer_1s();
			HAL_RTC_GetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
			HAL_RTC_GetDate(&hrtc, &currentDate, RTC_FORMAT_BIN);
			prog_Cyclic();

			if(TaskSem_T3){
				TaskSem_T3 = 0;
#ifdef APP_DEBUG  //-------------------------------------------------------
				if(dbg_btTaskCnt++ > 10){
					dbg_btTaskCnt = 0;

					for(uint8_t it = 0; it < sizeof(dbg_BTdataToSend)/sizeof(dbg_BTdataToSend[0]); it++){
						dbg_BTdataToSend[it]++;
					}

					if(dbg_btTaskSem){
						//uint8_t command[] = "AT+NAME";
						//JDY_sendCommand_AT(command, sizeof(command));
						JDY_sendData(dbg_BTdataToSend, sizeof(dbg_BTdataToSend)/sizeof(dbg_BTdataToSend[0]));
					}
					//bt_send();
				}
#endif			//-------------------------------------------------------

				// ADC conversion
				appStatus.batVoltage = ADC_ConvertBatVoltage(ADC_Data[adc_Battery]);
				appStatus.supVoltage = ADC_ConvertSupVoltage(ADC_Data[adc_Supply_33]);

				HAL_ADC_Start_DMA(&hadc1, (uint32_t*) &ADC_Data[0], 4);
			}


#ifdef APP_DEBUG //-------------------------------------------------------

			App_RunDebugRoutine();

			if(dbg_setTimeFlag){
				dbg_setTimeFlag = 0;
				HAL_RTC_SetTime(&hrtc, &startupTime, RTC_FORMAT_BIN);
			}
#endif			//-------------------------------------------------------

			break;
		case appMain_Error:
			break;
		case appMain_Halt:
			break;
		default:
			break;

		}

	} // Main while loop end
}

void App_UpdateMainTimer_1s(void){
	mainSystemTick = HAL_GetTick()/1000;
}


#ifdef APP_DEBUG  //-------------------------------------------------------

void App_RunDebugRoutine(void){

	if(HAL_GetTick() > (debugDelay + DEBG_DELAY)){
		debugDelay = HAL_GetTick();

		App_ToggleLed(Led1_dev, 5);

		// sweeping through LIN addresses in debug address table
		if(dbg_delayCounter++ > 20){
			dbg_delayCounter = 0;

			dbg_satIdGlobal = dbg_busIdTable[dbg_addresIdx];
			dbg_addresIdx++;

			//bt_send();
		}

		if(dbg_addresIdx > 2){
			dbg_addresIdx = 0;
		}


		if(dbg_command > 0){
			LIN_SendCommand(dbg_satIdGlobal, dbg_command, LIN_TXBuffer, 0);
		}
	}
}

#endif			//-------------------------------------------------------

void App_ToggleLed(device_e ledX_dev, uint8_t duty){

	if(appStatus.dutyCycles[ledX_dev] == 100){
		App_SetDuty(ledX_dev, duty);
	}
	else{
		App_SetDuty(ledX_dev, 0);
	}

}


void App_NVMInitiateApp(void){

	// Reading settings from NVM
	NVM_Entry_t serialEntry;

	//device ID
	NVM_ReadEntryById(nvm_Serial_Number, &serialEntry);
	appStatus.myAdress = (uint8_t) serialEntry.nvm_data[0];

	//reading LED initial duty cycles
	NVM_Entry_t ledEntry;
	NVM_ReadEntryById(nvm_Led_Duty, &ledEntry);

	for(uint8_t i = 0; i < LedCnt_MAX; i++){
		App_SetDuty(i, ledEntry.nvm_data[i]);
	}

	dev_InitSatAll_NVM();

}


void sys_InitADC(void){

	uint8_t memRead_L = 0;
	uint8_t memRead_H = 0;


	// reading Mains divider trim value from NVM
	NVM_Entry_t resTrim = {0};
	NVM_ReadEntryById(nvm_ResistorTrim, &resTrim);
	vResistorValue_H = resTrim.nvm_data[0];


	memRead_H = *((uint8_t*)V_REFL_ADDR);
	memRead_L = *((uint8_t*)V_REFH_ADDR);

	uint16_t reference = (uint16_t)(memRead_L<<8) + (uint16_t)(memRead_H);
	refFactor = (3.0 * reference)/4095;

	/*
	memRead_L = *((uint8_t*)TS_CAL1_L);
	memRead_H = *((uint8_t*)TS_CAL1_H);

	tsCal1 = (int32_t)(memRead_L<<8) + (int32_t)(memRead_H);

	memRead_L = *((uint8_t*)TS_CAL2_L);
	memRead_H = *((uint8_t*)TS_CAL2_H);

	tsCal2 = (int32_t)(memRead_L<<8) + (int32_t)(memRead_H);

	tempFactor = (float)(TS_CAL2_TEMP - TS_CAL1_TEMP) / ((int32_t)tsCal2 - (int32_t)tsCal1);
	*/
}

uint16_t ADC_ConvertSupVoltage(uint16_t rawVal){

	float rawF = 0;
	float voltage = 0;

	//conversion of ADC input
	// ADC_Data[3] is a measured internal reference voltage
	rawF = refFactor * rawVal/ADC_Data[adc_Reference];

	//conversion of voltage divider R = 62/ R1 = 10
	//U_we = (U_wy / R1)*(R + R1)
	voltage = (rawF * 2)*100;

	return (uint16_t) voltage;
}

uint16_t ADC_ConvertBatVoltage(uint16_t rawVal){

	float rawF = 0;
	float voltage = 0;

	//conversion of ADC input
	// ADC_Data[3] is a measured internal reference voltage
	rawF = refFactor * rawVal/ADC_Data[adc_Reference];

	//conversion of voltage divider R = 62/ R1 = 10
	//U_we = (U_wy / R1)*(R + R1)
	voltage = ((rawF * (vResistorValue_H + 10))/10)*100;


	return (uint16_t) voltage;
}

fxnResp_e App_SetDuty(device_e dev, uint8_t duty){
	fxnResp_e retVal = APP_NOK;

	if((duty >=0) && (duty <= 100)){
		// reversing duty - low side

		duty = 100 - duty;

		// resetting timer duty cycle
		TIM_SetDutyCycle(devList[dev].timer_p, devList[dev].timerChannel, duty);

		// updating application status register
		appStatus.dutyCycles[dev] = duty;

		if(duty > 0){
			APP_SET_BIT(appStatus.status.SR1_bitfields, dev);
		}
		else{
			APP_CLEAR_BIT(appStatus.status.SR1_bitfields, dev);
		}

		retVal = APP_OK;
	}
	return retVal;
}

fxnResp_e App_StopDev(device_e dev){
	fxnResp_e retVal = APP_NOK;

	App_SetDuty(dev, 0);
	retVal = APP_OK;

	return retVal;
}

fxnResp_e App_STOP_ALL(void){
	fxnResp_e retVal = APP_NOK;

	for(device_e i = Led1_dev; i < LedCnt_MAX; i++){
		App_StopDev(i);
	}

	retVal = APP_OK;
	return retVal;
}

void App_Tim3_Callback(TIM_HandleTypeDef *htim)
{
	TaskSem_T3++;
}
