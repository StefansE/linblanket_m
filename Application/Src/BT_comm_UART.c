/*
 * BT_comm_UART.c
 *
 *  Created on: Dec 27, 2023
 *      Author: ewars
 */

#include "main.h"
#include "gpio.h"
#include "usart.h"
#include "string.h"

#include "BT_Comm.h"
#include "BT_comm_UART.h"
#include "AppStatus.h"



uint8_t RxBuffer_BT[RX_BUFFER_SIZE] = {0};
uint8_t TxBuffer_BT[TX_BUFFER_SIZE] = {0};

uint8_t RxData_BT[RX_DATA_SIZE] = {0};
uint8_t TxData_BT[TX_DATA_SIZE] = {0};

void bt_sendBuffer_DMA(void);
void bt_ReceiveToIdleData_DMA();
void bt_CheckStatus(void);

BT_Dev_s BTGateway;

// Strange behavior of JDY-10
// Sending some garbage when connected for the first time
uint8_t first_S_unknown = 0;


// ################################################################
uint8_t bt_getNewRxFlag(void){
	return BTGateway.Status.RX_NewData;
}

uint8_t bt_getNewTxFlag(void){
	return BTGateway.Status.TX_NewData;
}

uint8_t bt_getRxDataSize(void){
	return BTGateway.RX_Xfer_Size;
}

uint8_t bt_getRxData(uint8_t * data){
	memcpy(data, BTGateway.Rx_Data_p, BTGateway.RX_Xfer_Size);
	return BTGateway.RX_Xfer_Size;
}

bt_mode_e bt_getMode(void){
	return BTGateway.bt_mode;
}


void bt_Init(UART_HandleTypeDef * huart){


	BTGateway.huart = huart;

	// Communication configuration
	// TX
	BTGateway.TX_Xfer_Size = 0;
	BTGateway.TX_Data_p = TxData_BT;
	BTGateway.TX_Buffer_p = TxBuffer_BT;

	// RX
	BTGateway.RX_Xfer_Size = 0;
	BTGateway.Rx_Data_p = RxData_BT;
	BTGateway.Rx_Buffer_p = RxBuffer_BT;

	// Setting up device pins
	// BT JDY-10 pin setup
	HAL_GPIO_WritePin(BT_RESET_GPIO_Port, BT_RESET_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(BT_PWRC_GPIO_Port, BT_PWRC_Pin, GPIO_PIN_SET);

	BTGateway.bt_mode = bt_mode_RESET;
	BTGateway.bt_state = bt_state_IDLE;
}

void bt_Cyclic(){

	// Check BT status based on PINs
	bt_CheckStatus();

	// BT main state machine
	switch(BTGateway.bt_mode){

		case bt_mode_RESET:
			// Init done, going directly to AT mode

			// Checking RESET PIN state
			if(HAL_GPIO_ReadPin(BT_RESET_GPIO_Port, BT_RESET_Pin) == GPIO_PIN_SET){
				bt_gotoMode(bt_mode_AT);
				//BTGateway.bt_mode = bt_mode_AT;
			}
			break;

		case bt_mode_AT:
			// Coming in from different mode
			break;

		case bt_mode_TRANSPARENT:
			// Coming in from different mode

			// Handle RX Data
			if(BTGateway.Status.RX_NewData){

				// Copy to data buffer if there is new data
				if(BTGateway.RX_Xfer_Count > 1){
					memcpy(BTGateway.Rx_Data_p, BTGateway.Rx_Buffer_p, BTGateway.RX_Xfer_Count);
					memset(BTGateway.Rx_Buffer_p, 0, RX_BUFFER_SIZE);
					BTGateway.RX_Xfer_Size = BTGateway.RX_Xfer_Count;
				}

				BTGateway.Status.RX_NewData = BIT_RESET;

				// Handle RX message
				// Done in upper layer - BT_Commm.
			}

			// Check if no data is waiting in RX and TX buffers
			if(BTGateway.Status.TX_NewData){
				// If gateway is ready - no transmission ongoing
				if(BTGateway.bt_state == bt_state_IDLE){
					bt_sendBuffer_DMA();
				}

			}
			else{
				// If not sending data
				if(BTGateway.bt_state == bt_state_IDLE){

					// Start DMA data receive chain
					if(BTGateway.Status.DataRx_DMA == BIT_RESET){
						BTGateway.Status.DataRx_DMA = BIT_SET;

						bt_ReceiveToIdleData_DMA();
					}
				}
			}
			break;

		default:
			break;

	}

}

void bt_gotoMode(bt_mode_e mode){

	switch(mode){
		case bt_mode_AT:
			HAL_GPIO_WritePin(BT_PWRC_GPIO_Port, BT_PWRC_Pin, GPIO_PIN_RESET);
			BTGateway.bt_mode = bt_mode_AT;
			break;

		case bt_mode_TRANSPARENT:
			HAL_GPIO_WritePin(BT_PWRC_GPIO_Port, BT_PWRC_Pin, GPIO_PIN_SET);
			BTGateway.bt_mode = bt_mode_TRANSPARENT;
			break;

		case bt_mode_RESET:
			HAL_GPIO_WritePin(BT_RESET_GPIO_Port, BT_RESET_Pin, GPIO_PIN_RESET);
			BTGateway.bt_mode = bt_mode_RESET;
			break;

		default:
			break;

	}

	// Going to RESET based on RESET PIN state
	if(HAL_GPIO_ReadPin(BT_RESET_GPIO_Port, BT_RESET_Pin) == GPIO_PIN_RESET){
		BTGateway.bt_mode = bt_mode_RESET;
	}
}


/*	-------------------------------------------------
 *
 * Starting DMA chain:
 * 1. Receiving first byte
 * 2. Going to "Normal" IRQ
 * 3. Staring "to IDLE" DMA transfer
 * 4. Going to "UARTEx" IRQ to finish transfer
 *
 --------------------------------------------------*/
void bt_ReceiveToIdleData_DMA(){

	// Starting DMA chain - receiving first byte
	HAL_UART_Receive_DMA(BTGateway.huart, BTGateway.Rx_Buffer_p, 1);

}

// IRQ on UARTEx requests - UARTEx_ReceiveToIdle_DMA
void bt_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size){
	// RX event handling
	if(BTGateway.bt_state == bt_state_Receiving){

		// DMA transfer is finished
		BTGateway.Status.DataRx_DMA = BIT_RESET;

		// There is new data in the buffer
		BTGateway.Status.RX_NewData = BIT_SET;

		// Updating Xfer size - Size + initial byte
		BTGateway.RX_Xfer_Count = Size + 1;

		// Update module status
		BTGateway.bt_state = bt_state_IDLE;

	}

	// TX event handling
	if(BTGateway.bt_state == bt_state_Sending){
		BTGateway.bt_state = bt_state_IDLE;
	}
}

void bt_UART_RxCpltCallback(UART_HandleTypeDef *huart){

	// JDY-10 sends "S" when connecting to phone
	// need to skip this first transmission
	if(first_S_unknown == 0){

		BTGateway.bt_state = bt_state_IDLE;
		BTGateway.Status.RX_NewData = BIT_RESET;
		BTGateway.RX_Xfer_Count = 0;
		BTGateway.Status.DataRx_DMA = BIT_RESET;

		first_S_unknown = 1;
	}

	// Normal mode after connection to JDY-10 is established
	else{
		BTGateway.bt_state = bt_state_Receiving;
		BTGateway.Status.DataRx_DMA = BIT_SET;
		BTGateway.RX_Xfer_Count = 1;

		HAL_UARTEx_ReceiveToIdle_DMA(BTGateway.huart, (BTGateway.Rx_Buffer_p + 1), RX_BUFFER_SIZE);
		// Disabling "Half Transfer Complete" IRQ
		__HAL_DMA_DISABLE_IT(BTGateway.huart->hdmarx,DMA_IT_HT);
	}

}


void bt_AT_Receive(uint8_t * command, uint8_t cmdSize, uint8_t * response){

	HAL_GPIO_WritePin(BT_PWRC_GPIO_Port, BT_PWRC_Pin, GPIO_PIN_RESET);


	HAL_UART_Transmit(BTGateway.huart, command, cmdSize, 10);
	//HAL_UART_Receive(&huart2, response, 10, 200);
	//HAL_UARTEx_ReceiveToIdle_DMA(&huart2, response, 4);
	HAL_UART_Receive_DMA(BTGateway.huart, response, 4);


	HAL_GPIO_WritePin(BT_PWRC_GPIO_Port, BT_PWRC_Pin, GPIO_PIN_SET);

}

void bt_sendBuffer_DMA(void){
	// Check if device is in TRANSPARENT mode (not connected)
	if(BTGateway.bt_mode == bt_mode_TRANSPARENT){

		// Copy to sending buffer
		memcpy(BTGateway.TX_Data_p, BTGateway.TX_Buffer_p, BTGateway.TX_Xfer_Size);

		// Double confirm setting PWRC PIN - exiting AT mode
		HAL_GPIO_WritePin(BT_PWRC_GPIO_Port, BT_PWRC_Pin, GPIO_PIN_SET);

		// Update DMA TX status flag
		BTGateway.Status.DataTx_DMA = BIT_SET;

		// Updating State
		BTGateway.bt_state = bt_state_Sending;

		// Start sending data
		HAL_UART_Transmit_DMA(BTGateway.huart, BTGateway.TX_Data_p, BTGateway.TX_Xfer_Size);
		// Disabling "Half Transfer Complete" IRQ
		__HAL_DMA_DISABLE_IT(BTGateway.huart->hdmatx,DMA_IT_HT);
	}
}


void bt_sendDataAsync(uint8_t * data, uint8_t size){

	if(size > 0){
		if(size < TX_DATA_SIZE){
			memcpy(BTGateway.TX_Buffer_p, data, size);
		}
		else{
			memcpy(BTGateway.TX_Buffer_p, data, TX_DATA_SIZE);
		}
		BTGateway.TX_Xfer_Size = size;
		BTGateway.Status.TX_NewData = BIT_SET;
	}

}

void bt_sendData_DMA(uint8_t * data, uint8_t size){

	// Check if device is in TRANSPARENT mode (not connected)
	if(BTGateway.bt_mode == bt_mode_TRANSPARENT){

		// Double confirm setting PWRC PIN - exiting AT mode
		HAL_GPIO_WritePin(BT_PWRC_GPIO_Port, BT_PWRC_Pin, GPIO_PIN_SET);

		// Updating State
		BTGateway.bt_state = bt_state_Sending;

		// Start sending data
		HAL_UART_Transmit_DMA(BTGateway.huart, data, size);
		// Disabling "Half Transfer Complete" IRQ
		__HAL_DMA_DISABLE_IT(BTGateway.huart->hdmatx,DMA_IT_HT);

	}
}

void bt_AT_sendCommand(uint8_t * command, uint8_t command_size){

	// Check if device is in AT mode (not connected)
	if(BTGateway.bt_mode == bt_mode_AT){

		// Check if there is no current transfer ongoing
		if(BTGateway.bt_state == bt_state_IDLE){

			BTGateway.RX_Xfer_Count = 0;

			uint32_t timeout_AT = HAL_GetTick();
			uint8_t endLine[] = "\n";

			BTGateway.bt_state = bt_state_Sending;

			//HAL_GPIO_WritePin(BT_PWRC_GPIO_Port, BT_PWRC_Pin, GPIO_PIN_RESET);
			bt_gotoMode(bt_mode_AT);

			// sending AT command
			HAL_UART_Transmit(BTGateway.huart, command, command_size, 10);
			// sending "end of line"
			HAL_UART_Transmit(BTGateway.huart, endLine, 1, 10);

			BTGateway.bt_state = bt_state_Receiving;

			// waiting for the response in blocking mode
			// "+" is always at the beginning of AT response
			while((*(BTGateway.Rx_Buffer_p + BTGateway.RX_Xfer_Count) != ASCII_PLUS) &&
					(HAL_GetTick() < timeout_AT + AT_RX_TIMEOUT)){
				HAL_UART_Receive(BTGateway.huart, (BTGateway.Rx_Buffer_p + BTGateway.RX_Xfer_Count), 1, 100);
			}

			// receiving response
			while((*(BTGateway.Rx_Buffer_p + BTGateway.RX_Xfer_Count) != ASCII_LF) &&
					(HAL_GetTick() < timeout_AT + AT_RX_TIMEOUT)){

				HAL_UART_Receive(BTGateway.huart, (BTGateway.Rx_Buffer_p + BTGateway.RX_Xfer_Count), 1, 100);
				BTGateway.RX_Xfer_Count++;
			}

			BTGateway.bt_state = bt_mode_RESET;

			// Copy data from temporary to permanent buffer
			if(BTGateway.RX_Xfer_Count > 0){
				memcpy(BTGateway.Rx_Data_p, BTGateway.Rx_Buffer_p, BTGateway.RX_Xfer_Count);
				BTGateway.Status.RX_NewData = BIT_SET;
			}

			//HAL_GPIO_WritePin(BT_PWRC_GPIO_Port, BT_PWRC_Pin, GPIO_PIN_SET);
			bt_gotoMode(bt_mode_TRANSPARENT);

		}
	}

}

void bt_CheckStatus(void){

	// Led 6
	uint8_t stat = HAL_GPIO_ReadPin(BT_STAT_GPIO_Port, BT_STAT_Pin);

	// Led 5
	uint8_t led1 = HAL_GPIO_ReadPin(BT_LED1_IN_GPIO_Port, BT_LED1_IN_Pin);

	// Led 4
	uint8_t out = HAL_GPIO_ReadPin(BT_OUT1_GPIO_Port, BT_OUT1_Pin);

	if(stat){
		App_SetDuty(Led6_dev, 5);
		APP_SET_BIT(BTGateway.Status.StatusRegister, STAT_LED);
		bt_gotoMode(bt_mode_TRANSPARENT);
		//BTGateway.bt_mode = bt_mode_TRANSPARENT;
	}
	else{
		App_StopDev(Led6_dev);
		APP_CLEAR_BIT(BTGateway.Status.StatusRegister, STAT_LED);
		bt_gotoMode(bt_mode_AT);
		//BTGateway.bt_mode = bt_mode_AT;
	}
	if(led1){
		App_SetDuty(Led5_dev, 5);
		APP_SET_BIT(BTGateway.Status.StatusRegister, LED1_LED);
	}
	else{
		App_StopDev(Led5_dev);
		APP_CLEAR_BIT(BTGateway.Status.StatusRegister, LED1_LED);
	}

	if(out){
		App_SetDuty(Led4_dev, 5);
		APP_SET_BIT(BTGateway.Status.StatusRegister, OUT_LED);
	}
	else{
		App_StopDev(Led4_dev);
		APP_CLEAR_BIT(BTGateway.Status.StatusRegister, OUT_LED);
	}


}

void bt_UART_TxCpltCallback(UART_HandleTypeDef *huart){

	// Frame transmitted
	BTGateway.Status.TX_NewData = BIT_RESET;

	// Reset size
	BTGateway.TX_Xfer_Size = 0;

	// Reset TX DMA flag
	BTGateway.Status.DataTx_DMA = BIT_RESET;

	// Update module state
	BTGateway.bt_state = bt_state_IDLE;

	// Start DMA data receive chain
	if(BTGateway.Status.DataRx_DMA == BIT_RESET){
		BTGateway.Status.DataRx_DMA = BIT_SET;

		bt_ReceiveToIdleData_DMA();
	}

}
