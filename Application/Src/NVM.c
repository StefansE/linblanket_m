/*
 * NVM.c
 *
 *  Created on: 22.02.2019
 *      Author: ewars
 */

#include "stdint-gcc.h"
#include "NVM.h"
#include "stm32g0xx_hal.h"
#include "stm32g0xx_hal_flash.h"


/*****************************************************************
 * 				Module macro definitions
 *****************************************************************/

#define NVM_PAGE_SIZE			2048
#define BYTE_DATA_ALLIGNMENT	8

//	PAGE 0
#define PAGE_0_START_ADDRES		0x0801F000
#define PAGE_0_END_ADDRES		((PAGE_0_START_ADDRES + NVM_PAGE_SIZE) - 1)

#define PAGE_0_DATA_START		(PAGE_0_START_ADDRES + BYTE_DATA_ALLIGNMENT)
#define PAGE_0_DATA_END			(PAGE_0_END_ADDRES - BYTE_DATA_ALLIGNMENT)

//	PAGE 1
#define PAGE_1_START_ADDRES		0x0801F800
#define PAGE_1_END_ADDRES		((PAGE_1_START_ADDRES + NVM_PAGE_SIZE) - 1)

#define PAGE_1_DATA_START		(PAGE_1_START_ADDRES + BYTE_DATA_ALLIGNMENT)
#define PAGE_1_DATA_END			(PAGE_1_END_ADDRES - BYTE_DATA_ALLIGNMENT)

/****************************************************************
 *				Type definitions
 ****************************************************************/

typedef enum {
	Page_Active = 0,
	Page_Ready,
	Page_Full,
	Page_NONE
} pageStatus_e;

typedef struct {
	struct {
		pageStatus_e 	pageStatus  :2;
		uint8_t			reserved	:6;
	}statusBits;

	uint8_t reserved;
	uint8_t reserved1;
} NVM_PageHeader_t;

typedef struct {
	NVM_PageHeader_t header;
	uint32_t * startAddr;
	uint32_t * endAddr;
	NVM_Entry_t * dataStart;
	NVM_Entry_t * dataEnd;
	NVM_Entry_t * empty_p;
} NVM_Page_t;



/**************************************************************
 *				Private function definitions
 **************************************************************/

static NVM_Page_t * 	FindActivePage(uint8_t * activePagesCount);
static uint8_t 			FindUpdateEmptySlot(NVM_Page_t * page);

static void 	PagesInit(void);
static void 	InitActivePage(NVM_Page_t * page);
static void 	InitHeader(NVM_Page_t * page);
static void 	WritePageHeader(NVM_Page_t * page);
static void 	ClearPage(NVM_Page_t * page);
static void 	SwapPage(NVM_Page_t * currentActivePage);

/*************************************************************
 *				Module global variables
 *************************************************************/

static NVM_Page_t Page0;
static NVM_Page_t Page1;

static NVM_Page_t * pageArray[] = {&Page0, &Page1};

// Mapping App_Device to NVM_Device
// !!!!!!!!!!	CHECK INDEXES	!!!!!!!!!!!!!!!!
//
static uint8_t nvm_DeviceArray[] = {nvm_Led_Duty,
									nvm_Serial_Number,
									nvm_ResistorTrim,
									nvm_satConfig_1,
									nvm_satConfig_2,
									nvm_satConfig_3,
									nvm_satConfig_4,
									nvm_satConfig_5,
									nvm_satConfig_6,
									nvm_satConfig_7,
									nvm_satConfig_8,
									nvm_satConfig_9,
									nvm_satConfig_10,
									nvm_satConfig_11,
									nvm_satConfig_12,
									nvm_satConfig_13,
									nvm_satConfig_14,
									nvm_satConfig_15,
									nvm_satConfig_16,
									nvm_satConfig_17,
									nvm_satConfig_18,
									nvm_satConfig_19,
									nvm_satConfig_20,
									nvm_satConfig_21,
									nvm_satConfig_22,
									nvm_satConfig_23,
									nvm_satConfig_24,
									nvm_satConfig_25};

/************************************************************
 *				Interface function bodies
 ************************************************************/

uint8_t NVM_GetDeviceEntryIndex(uint8_t device){
	if(device < sizeof(nvm_DeviceArray)/sizeof(nvm_DeviceArray[0])){
		return nvm_DeviceArray[device];
	}
	else return nvm_DeviceArray[0];
}

void NVM_ReadExtendedData(NVM_DataId_e entryId, uint8_t * data_p){
	NVM_Page_t * page_p;
	NVM_Entry_t * entry_p;

	uint8_t pageCount = 0;

	page_p = FindActivePage(&pageCount);

	entry_p = page_p->dataStart;

	while(entry_p <= page_p->dataEnd){

		if(entry_p->nvm_Id == entryId){
			*data_p = entry_p->nvm_extnd;
		}
		entry_p++;
	}
}

void NVM_ReadAll(NVM_Entry_t * entry){

	for(NVM_DataId_e id = 0; id < NVM_EntryCount; id++){

		NVM_ReadEntryById(id, entry+id);

	}
}

void NVM_ReadEntryById(NVM_DataId_e entryId, NVM_Entry_t * entry_p){
	NVM_Page_t * page_p;
	NVM_Entry_t * entryCurrent;

	uint8_t pageCount = 0;

	page_p = FindActivePage(&pageCount);

	entryCurrent = page_p->dataStart;

	while(entryCurrent <= page_p->dataEnd){

		if(entryCurrent->nvm_Id == entryId){
			entry_p->nvm_Id = entryCurrent->nvm_Id;
			entry_p->nvm_extnd = entryCurrent->nvm_extnd;

			// done due to different requirements regarding possible size of write to FLASH
			// in some cases it's 32bit write, in some 64bit ONLY
			for(uint8_t i = 0; i < NVM_CONTAINER_SIZE; i++){
				entry_p->nvm_data[i] = entryCurrent->nvm_data[i];
			}
		}
		entryCurrent++;
	}
}

static void SwapPage(NVM_Page_t * currentActivePage){

	NVM_Entry_t NVM_EntryTable[NVM_EntryCount];

	NVM_Entry_t * currentEntry_p = currentActivePage->dataStart;
	NVM_Page_t * pageReady_p;
	uint8_t i = 0;
	NVM_PageHeader_t * header;

	// update page swap marker
	if(*((uint8_t*) currentActivePage->endAddr) != Page_Full){

		HAL_FLASH_Unlock();

		HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t) currentActivePage->endAddr, Page_Full);

		HAL_FLASH_Lock();

	}

	// copy entries from NVM to temporary table
	while(currentEntry_p <= currentActivePage->dataEnd){

		NVM_EntryTable[currentEntry_p->nvm_Id].nvm_Id = currentEntry_p->nvm_Id;
		NVM_EntryTable[currentEntry_p->nvm_Id].nvm_extnd = currentEntry_p->nvm_extnd;

		for(uint8_t i = 0; i < NVM_CONTAINER_SIZE; i++){
			NVM_EntryTable[currentEntry_p->nvm_Id].nvm_data[i] = currentEntry_p->nvm_data[i];
		}

		currentEntry_p++;
	}

	// search for ready page
	for(i = 0; i < sizeof(pageArray)/sizeof(pageArray[0]); i++){
		header = (NVM_PageHeader_t *) pageArray[i]->startAddr;
		// check if there is any active page
		if(header->statusBits.pageStatus == Page_Ready){
			pageReady_p = pageArray[i];
		}
	}

	ClearPage(currentActivePage);
	InitHeader(currentActivePage);

	InitActivePage(pageReady_p);


	for(i = 0; i < sizeof(NVM_EntryTable)/sizeof(NVM_EntryTable[0]); i++){
		NVM_WriteEntry(&NVM_EntryTable[i]);
	}
}

void NVM_ClearAllPages(void){

	PagesInit();

	ClearPage(&Page0);
	ClearPage(&Page1);
}

void NVM_InitNVM(void){

	NVM_PageHeader_t * head;
	uint8_t i = 0;
	uint8_t isAnyPageActive = 0;

	PagesInit();

	// check if there is any active page
	FindActivePage(&isAnyPageActive);

	//initialize all not initialized pages
	for(i = 0; i < sizeof(pageArray)/sizeof(pageArray[0]); i++){

		head = (NVM_PageHeader_t *) pageArray[i]->startAddr;

		switch(head->statusBits.pageStatus){

			case Page_Active:

				// if page swap operation has not finished
				if(*((uint8_t *) pageArray[i]->endAddr) == Page_Full){
					SwapPage(pageArray[i]);
				}

				// in case there is more than one active partition
				if (isAnyPageActive > 1){
					// ERROR - more than one active page
				}
				break;

			case Page_Ready:
				break;

			default:

				// check for active pages
				FindActivePage(&isAnyPageActive);

				if(isAnyPageActive == 0){
					InitActivePage(pageArray[i]);


					// update active pages count
					FindActivePage(&isAnyPageActive);
				}
				else{
					InitHeader(pageArray[i]);
				}
				break;

		} // switch statement
	} //for loop
}

/************************************************************
 *					Private function bodies
 *************************************************************/

static void PagesInit(void){

	Page0.header.reserved = 0;
	Page0.startAddr 	= (uint32_t *) PAGE_0_START_ADDRES;
	Page0.endAddr 		= (uint32_t *) PAGE_0_END_ADDRES;
	Page0.dataStart		= (NVM_Entry_t *) PAGE_0_DATA_START;
	Page0.dataEnd		= (NVM_Entry_t *) PAGE_0_DATA_END;

	Page1.header.reserved = 0;
	Page1.startAddr		= (uint32_t *) PAGE_1_START_ADDRES;
	Page1.endAddr		= (uint32_t *) PAGE_1_END_ADDRES;
	Page1.dataStart		= (NVM_Entry_t *) PAGE_1_DATA_START;
	Page1.dataEnd		= (NVM_Entry_t *) PAGE_1_DATA_END;

}

/*
 * Finds Active page pointer and active pages count
 *
 * Return values:
 *
 * RetVal		Active Page Count		Return
 * 	!= 0			== 1				active page pointer
 * 	!= 0			 0					full page pointer - swap operation interrupted
 * 	== 0			 0					no active page found
 *
 */
static NVM_Page_t * FindActivePage(uint8_t * activePagesCount){

	NVM_Page_t * ret_p = 0;
	NVM_Page_t * full_p = 0;
	uint8_t i = 0, cnt = 0;
	NVM_PageHeader_t * head = 0;

	// search all page list
	for(i = 0; i < sizeof(pageArray)/sizeof(pageArray[0]); i++){

		head = (NVM_PageHeader_t *) pageArray[i]->startAddr;

		// check if there is any active page
		//if(pageArray[i]->header.statusBits.pageStatus == Page_Active){
		if( head->statusBits.pageStatus == Page_Active){
			cnt++;
			if(*((uint8_t*)pageArray[i]->endAddr) != Page_Full){
				ret_p = pageArray[i];
			}
			else{
				full_p = pageArray[i];
			}
		}
	}

	if(ret_p != 0){
		*activePagesCount = cnt;
		return ret_p;
	}
	else{
		*activePagesCount = 0;
		return full_p;
	}
}

static void InitActivePage(NVM_Page_t * page){


	ClearPage(page);

	page->header.statusBits.pageStatus = Page_Active;

	WritePageHeader(page);
}

static void InitHeader(NVM_Page_t * page){

	//initialize page
	ClearPage(page);

	page->header.statusBits.pageStatus = Page_Ready;

	WritePageHeader(page);
}

static void WritePageHeader(NVM_Page_t * page){

	uint16_t * data = (uint16_t *) &page->header;

	HAL_FLASH_Unlock();

	HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t) page->startAddr, *data);

	HAL_FLASH_Lock();

}

static void ClearPage(NVM_Page_t * page){

	FLASH_EraseInitTypeDef erase;
	uint32_t error;

	erase.TypeErase = 0x00;
	erase.NbPages = 1;
	erase.Page = (uint32_t) page->startAddr;

	HAL_FLASH_Unlock();

	HAL_FLASHEx_Erase(&erase, &error);

	HAL_FLASH_Lock();
}


void NVM_WriteEntry(NVM_Entry_t * entry){

	uint8_t 	pageSwapTriger = 0;
	uint8_t 	activePageCount = 0;
	NVM_Page_t * activePage_p;

	activePage_p = FindActivePage(&activePageCount);

	if(activePageCount == 1){
		pageSwapTriger = FindUpdateEmptySlot(activePage_p);

		HAL_FLASH_Unlock();

		HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t) activePage_p->empty_p, (uint64_t) entry->nvm_Id);

		HAL_FLASH_Lock();
	}
	else{
		// ERROR - more than one active pages found
		for(;;){}
	}

	if(0 != pageSwapTriger){
		SwapPage(activePage_p);
	}
}

static uint8_t FindUpdateEmptySlot(NVM_Page_t * page){

	uint8_t retVal = 0;
	NVM_Entry_t * current_p = page->dataStart;

	while((current_p->nvm_Id != 0xFF) && (current_p != page->dataEnd)){
		current_p++;
	}

	if(current_p >= page->dataEnd){
		retVal = 1;
	}

	page->empty_p = current_p;

	return retVal;
}
