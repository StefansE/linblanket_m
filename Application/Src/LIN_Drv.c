/*
 * LIN_Service.c
 *
 *  Created on: 22 gru 2022
 *      Author: ewars
 */

#include "string.h"

#include <LIN_Drv.h>
#include "main.h"

#include "LIN_command.h"
#include "AppStatus.h"

#define LIN_SYNC_BYTE 		0x55
#define LIN_RX_TX_TIMEOUT	10

#define LIN_HEADER_SIZE		5



typedef struct {
	uint8_t serviceID;
	uint8_t respLen;
} lin_service_s;

/*	--------	Function prototypes	-----------	*/
static void 			LINDrv_UART_Transmit_DMA(void);
static void 			LinDrv_SendCommand(void);

static uint8_t 			LINDrv_ID_parity(uint8_t linID);
static uint8_t 			LINDrv_HeaderChecksum(LIN_header_t * header);

static lin_service_s 	LINDrv_FindService(uint8_t serviceID);

/*		Unused 		*/
//static uint8_t 			LINDrv_DataChecksum(uint8_t * data_p, uint8_t len);

/*	--------	Variable Definition	-----------	*/

const lin_service_s linCommandTable[] = {
		{LIN_CMD_RESERVED, 			0},//to align with CMD numbers

		{LIN_CMD_GET_STATUS, 			LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMD_GET_MY_ADDRESS,		LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMD_GET_OUT_DUTY,			LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMD_GET_LED1_DUTY,			LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMD_GET_LED2_DUTY,		 	LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMD_GET_BAT_VOLTAGE,		LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMD_GET_SUP_VOLTAGE,		LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMG_GET_CHIP_ID_0,			LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMG_GET_CHIP_ID_1,		 	LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMG_GET_CHIP_ID_2,			LIN_DEFAULT_PAYLOAD_SIZE},
		{LIN_CMD_GET_TEMPERATURE,		LIN_DEFAULT_PAYLOAD_SIZE},

		{LIN_CMD_SET_MOTOR_DUTY,		0},
		{LIN_CMD_SET_LED1_DUTY,			0},
		{LIN_CMD_SET_LED2_DUTY,			0},
		{LIN_CMD_SET_MOTOR_INIT,		0},
		{LIN_CMD_SET_LED1_INIT,			0},
		{LIN_CMD_SET_LED2_INIT,			0},
		{LIN_CMD_SET_MY_ADRESS,			0},
		{LIN_CMD_SET_MY_ROLE,			0},
		{LIN_START_MOTOR,				0},
		{LIN_STOP_MOTOR,				0},
		{LIN_CMD_SET_RESISTOR_TRIM,		0},

		{LIN_ZONE_START,				0},
		{LIN_ZONE_STOP,					0}
};


uint8_t LIN_RX_Buffer[LIN_RX_BUFFER_SIZE];
uint8_t LIN_RX_Data[LIN_RX_BUFFER_SIZE];

uint8_t LIN_TX_Buffer[LIN_RX_BUFFER_SIZE];
uint8_t LIN_TX_Data[LIN_RX_BUFFER_SIZE];

LinDev_s LinDev;

/*	###########################################################################	*/

void LinDrv_Init(UART_HandleTypeDef* huart){

	//Initializing LIN device
	LinDev.huart = huart;

	LinDev.RX_Buffer_p = LIN_RX_Buffer;
	LinDev.RX_Data_p = LIN_RX_Data;

	LinDev.TX_Buffer_p = LIN_TX_Buffer;
	LinDev.TX_Data_p = LIN_TX_Data;

	// Initializing LIN header
	LinDev.Tx_Header_Buffer.Synchronization_Byte_u8 = LIN_SYNC_BYTE;

	// Updating device state after initialization
	LinDev.State = lin_st_IDLE;
}


void LinDrv_Cyclic(void){

	switch(LinDev.State){

	case lin_st_INIT:
		LinDrv_Init(&huart1);
		break;

	case lin_st_IDLE:
		// Check for TX messages
		if(LinDev.Status.TX_NewData){
			LinDrv_SendCommand();
		}

		// There is new data in RX buffer
		if(LinDev.Status.RX_NewData == BIT_SET){
			// Copy data to Data buffer
			memcpy(LinDev.RX_Data_p, LinDev.RX_Buffer_p, LinDev.RX_Xfer_Size);

			// Reset new data bit
			LinDev.Status.RX_NewData = BIT_RESET;
		}

		break;

	case lin_st_Sending:
		break;

	case lin_st_Receiving:
		break;

	default:
		LinDev.State = lin_st_INIT;
		break;

	}
}

uint8_t LinDrv_GetResponse(uint8_t* data_p){

	uint8_t size = 0;

	// If there is new data in the buffer
	if(LinDev.Status.RX_NewData){

		size = LinDev.RX_Xfer_Size;

		// Copying data
		memcpy(data_p, LinDev.RX_Data_p, LinDev.RX_Xfer_Size);

		// Clear flags
		LinDev.Status.RX_NewData = BIT_RESET;
		LinDev.RX_Xfer_Size = 0;
	}

	return size;
}

void LinDrv_SendDeviceCommand(linMessage_s message, uint8_t dataSize){

	// Copying data to TX header
	LinDev.Tx_Header_Buffer.Identifier_u8 = message.devID;
	LinDev.Tx_Header_Buffer.Command_u8 = message.commandID;
	// Setting up size of data
	LinDev.Tx_Header_Buffer.MsgSize_u8 = dataSize;
	// Additional data length
	LinDev.TX_Xfer_Size = LIN_HEADER_SIZE + dataSize;

	// Recalculating Header message header
	LinDev.Tx_Header_Buffer.Identifier_u8 |= LINDrv_ID_parity(LinDev.Tx_Header_Buffer.Identifier_u8);
	LinDev.Tx_Header_Buffer.Checksum_u8 = LINDrv_HeaderChecksum(&LinDev.Tx_Header_Buffer);

	if(LinDev.Tx_Header_Buffer.MsgSize_u8 > 0){
		// Copying data to buffer
		memcpy(LinDev.TX_Buffer_p, message.data_p, LinDev.Tx_Header_Buffer.MsgSize_u8);
	}

	// Requesting to send data
	LinDev.Status.TX_NewData = BIT_SET;
}

void LinDrv_SendCommand(void){

	if(LinDev.TX_Xfer_Size > 0){

		// Copying data to TX_header buffer
		memcpy(&LinDev.Tx_Header_Data, &LinDev.Tx_Header_Buffer, (sizeof(LinDev.Tx_Header_Buffer)/sizeof(LinDev.Tx_Header_Buffer.Synchronization_Byte_u8)));

		// Copying data to TX_Data buffer
		memcpy(LinDev.TX_Data_p, LinDev.TX_Buffer_p, LinDev.TX_Xfer_Size);

		// Starting DMA communication chain
		LINDrv_UART_Transmit_DMA();

	}
}


/*
 * 		<LIN_GetDeviceStatus>
 * 		devId:			Device LIN bus address
 * 		rxBuffer:		pointer to device status structure
 *
 *		expects 4 bytes response to fill devStatus_s:
 *			1. status bits,
 *			2. motor duty,
 *			3. led1 duty,
 *			4. led2 duty
 */
void LIN_GetDeviceStatus(uint8_t devId, devStatus_s * devStatus){
	LIN_header_t header = {0};
	header.Identifier_u8 = devId;
	header.Command_u8 = 1;
	header.MsgSize_u8 = LIN_DEFAULT_PAYLOAD_SIZE;


	LIN_ReadData(&header, (uint8_t *) devStatus, header.MsgSize_u8);
}

/*
 *		<LIN_WriteData>
 *		header:		pointer to LIN message header
 *		Txdata_p:		pointer to Tx data set to 4 bytes
 */
void LIN_WriteData(LIN_header_t  * header, uint8_t * TXdata_p){

	header->Synchronization_Byte_u8 = 0x55;
	header->Identifier_u8 |= LINDrv_ID_parity(header->Identifier_u8);
	header->Checksum_u8 = LINDrv_HeaderChecksum(header);
	// set to 4 by default
	header->MsgSize_u8 = 4;

	HAL_LIN_SendBreak(&huart1);

	// Transmit header
	HAL_UART_Transmit(&huart1, (uint8_t *) header, 5, 100);
	// Transmit data
	HAL_UART_Transmit(&huart1, (uint8_t *) TXdata_p, header->MsgSize_u8, 100);
}


void LIN_ReadData(LIN_header_t *header, uint8_t * RXdata_p, uint8_t size){

	header->Synchronization_Byte_u8 = 0x55;
	header->Identifier_u8 |= LINDrv_ID_parity(header->Identifier_u8);
	header->MsgSize_u8 = size;
	header->Checksum_u8 = LINDrv_HeaderChecksum(header);

	ATOMIC_SETH_BIT(huart1.Instance->CR2, USART_CR2_LINEN);

	// Starting LIN communication
	HAL_LIN_SendBreak(&huart1);

	// Transmit header
	HAL_UART_Transmit(&huart1, (uint8_t *) header, 5, 10);
	// Read 10 bytes of return data:
	// initial "0" + 5 header bytes + 4 reply bytes
	HAL_UART_Receive(&huart1, RXdata_p, LIN_DEFAULT_RECEIVE_SIZE, 10);
}

/*
uint8_t LINDrv_DataChecksum(uint8_t * data_p, uint8_t len){
	uint16_t ckSum = 0;
	uint8_t i = 0;
	uint8_t * ptr = data_p;

	for(i = 0; i < len; i++){
		ckSum += *ptr;
		if(ckSum > 0xFF){
			ckSum -= 0xFF;
		}
		ptr++;
	}

	// XOR with FF - reverse
	ckSum = ckSum ^ 0xFF;

	return (uint8_t)ckSum;
}
*/

uint8_t LINDrv_HeaderChecksum(LIN_header_t * header){
	uint16_t ckSum = 0;
	uint8_t i = 0;
	uint8_t * data_p = (uint8_t*) header;

	for(i = 0; i < 4; i++){
		ckSum += *(data_p + i);
		if(ckSum > 0xFF){
			ckSum -= 0xFF;
		}
	}

	// XOR with FF - reverse
	ckSum = ckSum ^ 0xFF;

	return ckSum;
}

uint8_t LINDrv_ID_parity(uint8_t linID){
	uint8_t parity = 0;
	uint8_t parL, parH;

	uint8_t parityLowBytes[] = {0, 1, 2, 4};
	uint8_t parityHighBytes[] = {1, 3, 4, 5};

	//Parity LOW bit
	parL = 0;
	for(uint8_t i = 0; i < 4; i++){
		if(linID & (1<<parityLowBytes[i])){
			parL++;
		}
	}
	//Parity High bit
	parH = 0;
	for(uint8_t i = 0; i < 4; i++){
		if(linID & (1<<parityHighBytes[i])){
			parH++;
		}
	}

	parH = !parH;

	parity = ((parL & 0x01)<<6) + ((parH & 0x01)<<7);

	return parity;
}


void LINDrv_UART_Transmit_DMA(void){

	lin_service_s service = LINDrv_FindService(LinDev.Tx_Header_Buffer.Command_u8);
	LinDev.RX_Xfer_Size = service.respLen;

	LinDev.State = lin_st_Sending;

	LinDev.Status.TX_DMAxfer = BIT_SET;

	// MessageSize = header size + data size
	uint8_t msgSize = (sizeof(LinDev.Tx_Header_Buffer)/sizeof(LinDev.Tx_Header_Buffer.Synchronization_Byte_u8)) + LinDev.TX_Xfer_Size;

	uint8_t msgBuf[msgSize];

	memcpy(msgBuf, &LinDev.Tx_Header_Data, (sizeof(LinDev.Tx_Header_Buffer)/sizeof(LinDev.Tx_Header_Buffer.Synchronization_Byte_u8)));
	memcpy((msgBuf + (sizeof(LinDev.Tx_Header_Buffer)/sizeof(LinDev.Tx_Header_Buffer.Synchronization_Byte_u8))), LinDev.TX_Data_p, LinDev.TX_Xfer_Size);

	HAL_LIN_SendBreak(LinDev.huart);

	HAL_UART_Transmit_DMA(LinDev.huart, msgBuf, msgSize);
	// Disabling "Half Transfer Complete" IRQ
	__HAL_DMA_DISABLE_IT(LinDev.huart->hdmatx,DMA_IT_HT);

}


void Lin_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	switch(LinDev.State){

		// Coming from DMA sending
		case lin_st_Sending:
			// Sending is done
			LinDev.Status.TX_DMAxfer = BIT_RESET;

			// Resetting TX Xfer size
			LinDev.TX_Xfer_Size = 0;

			// Resetting TX new data bit
			LinDev.Status.TX_NewData = BIT_RESET;

			//There is something to be received
			if(LinDev.RX_Xfer_Size > 0){

				LinDev.State = lin_st_Receiving;
				LinDev.Status.RX_DMAxfer = BIT_SET;

				HAL_UARTEx_ReceiveToIdle_DMA(LinDev.huart, LinDev.RX_Buffer_p, LinDev.RX_Xfer_Size);
				// Disabling "Half Transfer Complete" IRQ
				__HAL_DMA_DISABLE_IT(LinDev.huart->hdmarx,DMA_IT_HT);

				// Updating state
				LinDev.State = lin_st_Receiving;
			}
			break;

		// Coming from DMA receiving
		case lin_st_Receiving:
			// Copying data to Data buffer
			memcpy(LinDev.RX_Data_p, LinDev.RX_Buffer_p, LinDev.RX_Xfer_Size);

			// Update device state
			LinDev.Status.RX_NewData = BIT_SET;
			LinDev.State = lin_st_IDLE;
			break;

		default:
			// TODO
			// Clear all flags?
			// Abort all operations?
			break;

	}
}

void Lin_UART_RxCpltCallback(UART_HandleTypeDef *huart){

}

void Lin_UARTEx_RxEventCallback(UART_HandleTypeDef* huart, uint16_t Size){

	if(LinDev.State == lin_st_Receiving){
		// End of DMA reception or IDLE
		LinDev.Status.RX_DMAxfer = BIT_RESET;
		// Size of received message
		LinDev.RX_Xfer_Size = Size;
		// New data in RX buffer
		LinDev.Status.RX_NewData = BIT_SET;

		// Switch back to IDLE
		LinDev.State = lin_st_IDLE;
	}

}

lin_service_s LINDrv_FindService(uint8_t serviceID){

	for(uint8_t i = 0; i < sizeof(linCommandTable)/sizeof(linCommandTable[0]); i++){
		if(linCommandTable[i].serviceID == serviceID){
			return linCommandTable[i];
		}
	}
	// Return default - GET SATATUS
	return linCommandTable[1];
}


