/*
 * devSetup.c
 *
 *  Created on: 21 paź 2023
 *      Author: ewars
 */


#include "LIN_command.h"
#include "devSetup.h"

// main runtime structure for satellites
devSattelite_s satDevList[devZone_MAX_ALL][devP_MAX_ALL] = {0};

uint8_t dev_satFindAll(void){

	uint8_t satcount = 0;

	for(devZone_e z = devZone_collar; z < devZone_MAX_ALL; z++){
		for(devPos_e p = devP_Left; p < devP_MAX_ALL; p++){

			uint8_t devId = (10 * z) + p;
			devStatus_s devStatus = {0};

			while(devStatus.syncByte != 0x55){
				LIN_GetDeviceStatus(devId, &devStatus);
			}

			uint8_t ds = devStatus.led1Duty + devStatus.led2Duty + devStatus.motorDuty + devStatus.statusBits;

			if(ds){
				satcount ++;
			}

		}
	}
	return satcount;
}


void dev_ScanForSatellites(void){

	for(devZone_e z = devZone_collar; z < devZone_MAX_ALL; z++){
		for(devPos_e p = devP_Left; p < devP_MAX_ALL; p++){
			uint8_t devId = (10 * z) + p;
			devStatus_s devStatus = {0};

			while(devStatus.syncByte != 0x55){
				LIN_GetDeviceStatus(devId, &devStatus);
			}

			uint8_t ds = devStatus.led1Duty + devStatus.led2Duty + devStatus.motorDuty + devStatus.statusBits;

			if(ds){
				devSattelite_s * sat_p = &satDevList[z][p];

				sat_p->Led1_duty = devStatus.led1Duty;
				sat_p->Led2_duty = devStatus.led2Duty;
				sat_p->Dev_duty = devStatus.motorDuty;
				sat_p->devBusAddres = devId;
				sat_p->devPosition = p;
				sat_p->devZone = z;
				sat_p->devStatus = devSts_Active;
			}

		}
	}
}


void dev_InitSat_NVM(devZone_e devZone, devPos_e devPos){

	uint8_t id = nvm_satConfig_1 + (devZone_MAX_ALL * devZone) + devPos;
	NVM_Entry_t entry = {0};
	devSattelite_s * sat_p = &satDevList[devZone][devPos];
	uint8_t * satDev_p = &satDevList[devZone][devPos].devZone;

	NVM_ReadEntryById(id, &entry);

	sat_p->devStatus = entry.nvm_extnd;
	for(uint8_t i = 0; i < NVM_CONTAINER_SIZE; i++){
		*(satDev_p + i) = entry.nvm_data[i];
	}
}

void dev_InitSatAll_NVM(void){

	for(uint8_t i = 0; i < devZone_MAX_ALL; i++){
		for(uint8_t j = 0; j < devP_MAX_ALL; j++){
			dev_InitSat_NVM(i, j);
		}
	}
}

fxnResp_e dev_setSatAddress(devZone_e devZone, devPos_e devPos){
	fxnResp_e resp = APP_NOK;

	LIN_header_t header = {0};
	header.Identifier_u8 = DEV_NOT_ACITVE_BUS_ID;
	header.MsgSize_u8 = LIN_DEFAULT_PAYLOAD_SIZE;
	header.Command_u8 = LIN_CMD_SET_MY_ADRESS;

	uint8_t txBuff[4] = {0};
	txBuff[0] = ((uint8_t) devZone * 10) + (uint8_t) devPos;

	LIN_WriteData(&header, txBuff);

	devStatus_s status = {0};

	LIN_GetDeviceStatus(txBuff[0], &status);

	if(status.devID == txBuff[0]){
		resp = APP_OK;
	}

	return resp;

}

fxnResp_e dev_dismissSat(devZone_e devZone, devPos_e devPos){
	fxnResp_e resp = APP_NOK;

	LIN_header_t header = {0};
	//header.Identifier_u8 = satDevList[devZone][devPos].devBusAddres;
	header.Identifier_u8 = (devZone * 10) + devPos;
	//header.Identifier_u8 = 19;
	header.MsgSize_u8 = LIN_DEFAULT_PAYLOAD_SIZE;
	header.Command_u8 = LIN_CMD_SET_MY_ADRESS;

	uint8_t txBuff[4] = {0};
	txBuff[0] = DEV_NOT_ACITVE_BUS_ID; // Zone 6, Position 3: indicates sat not initiated

	LIN_WriteData(&header, txBuff);

	devStatus_s status = {0};

	LIN_GetDeviceStatus(txBuff[0], &status);

	if(status.devID == DEV_NOT_ACITVE_BUS_ID){
		resp = APP_OK;
	}

	return resp;
}

fxnResp_e dev_UpdateSat_MDuty(devZone_e devZone, devPos_e devPos, uint8_t duty){

	fxnResp_e resp = APP_NOK;
	devSattelite_s * sat_p = &satDevList[devZone][devPos];
	LIN_header_t header = {0};

	// update local list
	sat_p->Dev_duty = duty;

	// send LIN message
	header.Identifier_u8 = sat_p->devBusAddres;
	header.MsgSize_u8 = LIN_DEFAULT_PAYLOAD_SIZE;
	header.Command_u8 = LIN_CMD_SET_MOTOR_DUTY;

	LIN_WriteData(&header, &sat_p->Led1_duty);

	// check if update was successful
	devStatus_s status = {0};

	LIN_GetDeviceStatus(sat_p->devBusAddres, &status);

	if(status.motorDuty == duty){
		resp = APP_OK;
	}

	return resp;

}

void dev_UpdateZoneMotorDuty(devZone_e devZone, uint8_t duty){
	// update local list
	for(uint8_t i = 0; i < devP_MAX_ALL; i++){
		satDevList[devZone][i].Dev_duty = duty;
	}

	// send LIN mass message
	LIN_header_t header = {0};
	header.Identifier_u8 = ZONE_ADDRESS(devZone);
	header.MsgSize_u8 = LIN_DEFAULT_PAYLOAD_SIZE;
	header.Command_u8 = LIN_CMD_SET_MOTOR_DUTY;

	LIN_WriteData(&header, &satDevList[devZone][devP_Left].Led1_duty);

	/*TODO*/
	// check if update was successful
}

void dev_LinSend(uint8_t devId, uint8_t command, uint8_t * dataP){

	LIN_header_t head = {0};

	head.Identifier_u8 = devId;
	head.Command_u8 = command;

	LIN_WriteData(&head, dataP);
}

void dev_LinReceive(uint8_t devId, uint8_t command, uint8_t * dataP){

	LIN_header_t head = {0};

	head.Identifier_u8 = devId;
	head.Command_u8 = command;

	LIN_ReadData(&head, dataP, LIN_DEFAULT_PAYLOAD_SIZE);
}

