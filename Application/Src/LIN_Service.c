/*
 * LIN_Service.c
 *
 *  Created on: Jan 6, 2024
 *      Author: ewars
 */


#include "LIN_Drv.h"
#include "LIN_Service.h"

uint8_t LinServNewData = 0;
uint8_t LinServ_RxData[20] = {0};

void LIN_Init(UART_HandleTypeDef* huart){
	LinDrv_Init(huart);
}

void LIN_Cyclic(void){
	LinDrv_Cyclic();

	// Checking for response, clearing buffer
	uint8_t respSize = LinDrv_GetResponse(LinServ_RxData);

	if(respSize > 0){
		LinServNewData++;
		// Do what's needed
	}

}

void LIN_SendCommand(uint8_t devId, uint8_t command, uint8_t* txData_p, uint8_t Size){

	linMessage_s msg;

	msg.commandID = command;
	msg.devID = devId;
	msg.data_p = txData_p;

	LinDrv_SendDeviceCommand(msg, Size);
}
