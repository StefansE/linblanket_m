/*
 * Program_NVM.c
 *
 *  Created on: Nov 17, 2023
 *      Author: ewars
 */


#include "Program_NVM.h"

//	PAGE 56
#define	FIRST_PROGRAM_POINTER	(0x0801C000)
#define PROGRAM_QUEUE_END		0

#define RUN_STEPS_MAX_COUNT		100
#define PROGRAM_MAX_COUNT		100

pProgram_s *	runProgList[PROGRAM_MAX_COUNT] = {0};
stepConfig_s 	runtimeSteps[RUN_STEPS_MAX_COUNT] = {0};


//	read list of available programs from NVM
void pNvm_initNvm(pProgram_s * runTimeProgramList){

	// setting pointer to first program available in NVM
	pProgram_s * progPointer_p = (pProgram_s *) FIRST_PROGRAM_POINTER;

	// do while pointer to next program is different from "End Queue" pattern
	while(PROGRAM_QUEUE_END != progPointer_p->nextProgram_p){
		// update runtime program list
		runTimeProgramList = progPointer_p;
		// set pointer to next program available in NVM
		progPointer_p = (pProgram_s *) progPointer_p->nextProgram_p;
		// update array pointer
		runTimeProgramList++;
	}

}

void prog_LoadProg(pProgram_s * pProgram_p){

	uint8_t i = 0;

	for(stepConfig_s * step = pProgram_p->firstStep_p; step <= pProgram_p->lastStep_p; step++){
		runtimeSteps[i] = *(step);
		i++;

		if(i >= RUN_STEPS_MAX_COUNT){
			break;
		}

	}
}

void prog_Cyclic(void){}

void prog_Run(void){}
void prog_Stop(void){}




